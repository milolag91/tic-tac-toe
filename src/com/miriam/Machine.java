package com.miriam;

public class Machine {
    Game game;

    Machine(Game game) {
        this.game = game;
    }
    public void move(int player) {
        int[] map = game.getMap();

        if (map[4] == 0) {
            game.rawSetMap(4, player);
            return;
        }
        for(int i = 0; i < Game.winnerPlays.length; i++) {
            int totalPlayer2 = 0;
            for (int j = 0; j < Game.winnerPlays[0].length; j++) {
                int currentCell = Game.winnerPlays[i][j] - 1;
                if (Game.map[currentCell] == player) {
                    totalPlayer2++;
                }
                if (totalPlayer2 == 2) {
                    for (int k = 0; k < Game.winnerPlays[0].length; k++) {
                        int winCell = Game.winnerPlays[i][k] - 1;
                        if (Game.map[winCell] == 0) {
                            game.rawSetMap(winCell, player);
                            return;
                        }
                    }
                }
            }
        }
        for(int i = 0; i < Game.winnerPlays.length; i++) {
            int totalPlayer1 = 0;
            for (int j = 0; j < Game.winnerPlays[0].length; j++) {
                int currentCell = Game.winnerPlays[i][j] - 1;
                if (Game.map[currentCell] == opponentPlayer(player)) {
                    totalPlayer1++;
                }
                if (totalPlayer1 == 2) {
                    for (int k = 0; k < Game.winnerPlays[0].length; k++) {
                        int winCell = Game.winnerPlays[i][k] - 1;
                        if (Game.map[winCell] == 0) {
                            game.rawSetMap(winCell, player);
                            return;
                        }
                    }
                }
            }
        }
        for(int i = 0; i < Game.winnerPlays.length; i++) {
            for (int j = 0; j < Game.winnerPlays[0].length; j++) {
                int winCell = Game.winnerPlays[i][j] - 1;
                if (Game.map[winCell] == 0) {
                    game.rawSetMap(winCell, player);
                    return;
                }
            }
        }
    }

    protected int opponentPlayer(int player){
        if(player == Game.player1) {
            return Game.player2;
        }else {
            return Game.player1;
        }
    }
}
