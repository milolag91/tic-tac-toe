package com.miriam;

import java.util.Scanner;

public class Game {
    Scanner keyboard = new Scanner(System.in);
    public static int[] map = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    public final static int player1 = 1;
    public final static int player2 = 2;
    private int actualPlayer = 1;
    public final static int[][] winnerPlays = new int[][]{
            new int[]{1, 2, 3},
            new int[]{4, 5, 6},
            new int[]{7, 8, 9},
            new int[]{1, 4, 7},
            new int[]{2, 5, 8},
            new int[]{3, 6, 9},
            new int[]{1, 5, 9},
            new int[]{3, 5, 7},
    };


    protected void play1vs1() {
        boolean GameLoop = true;
        while (GameLoop) {
            drawBoard();
            input1vs1(actualPlayer);
            int winner = checkWinner();
            if(winner == player1 || winner == player2) {
                drawBoard();
                System.out.printf("Ha ganado el jugador %d\n", winner);
                GameLoop = false;
                break;
            }
            if (checkDraw()) {
                drawBoard();
                System.out.println("Empate");
                GameLoop = false;
                break;
            }
            switchPlayer();
        }
    }
    protected void playVsMachine() {
        Machine machine = new Machine(this);
        boolean GameLoop = true;
        while (GameLoop) {
            drawBoard();
            if (actualPlayer == player1) {
                input1vs1(actualPlayer);
            } else {
                System.out.println("Máquina:");
                machine.move(player2);
            }
            int winner = checkWinner();
            if(winner == player1 || winner == player2) {
                drawBoard();
                if(winner == player1) {
                    System.out.printf("Ha ganado el jugador %d\n", winner);
                    GameLoop = false;
                    break;
                }
                if(winner == player2) {
                    System.out.printf("Ha ganado la máquina");
                    GameLoop = false;
                    break;
                }
            }
            if (checkDraw()) {
                drawBoard();
                System.out.println("Empate");
                GameLoop = false;
                break;
            }
            switchPlayer();
        }
    }
    protected void playVsMachines() {
        Machine machine = new Machine(this);
        boolean GameLoop = true;
        while (GameLoop) {
            drawBoard();
            if (actualPlayer == player1) {
                System.out.println("Máquina 1:");
                machine.move(player1);
            } else {
                System.out.println("Máquina 2:");
                machine.move(player2);
            }
            int winner = checkWinner();
            if(winner == player1 || winner == player2) {
                drawBoard();
                System.out.printf("Ha ganado la máquina %d\n", winner);
                GameLoop = false;
                break;
            }
            if (checkDraw()) {
                drawBoard();
                System.out.println("Empate");
                GameLoop = false;
                break;
            }
            switchPlayer();
        }
    }

    public boolean validatePosition(String input) {
        int parseInput;
        try {
            parseInput = Integer.parseInt(input);
        } catch (Exception e) {
            System.out.println("No metas texto");
            return false;
        }

        if (parseInput < 1 || parseInput > 9) {
            System.out.println("Número no válido.");
            return false;
        }
        if (map[parseInput - 1] != 0) {
            System.out.println("Esa posición ya está ocupada.");
            return false;
        }
        return true;
    }

    public boolean validateMainMenu(String input) {
        int parseInput;
        try {
            parseInput = Integer.parseInt(input);
        } catch (Exception e) {
            System.out.println("No metas texto");
            return false;
        }

        if (parseInput < 1 || parseInput > 4) {
            System.out.println("Número no válido.");
            return false;
        }
        return true;
    }

    public void setMap(int position, int player) {
            map[position - 1] = player;
    }

    public  void rawSetMap(int position, int player) {
        map[position] = player;
    }
    public int[] getMap() {
        return map;
    }

    public void drawBoard() {
        String result = "";
        for (int i = 0; i < map.length; i++) {
            switch (map[i]) {
                case 0:
                    result += " - ";
                    break;
                case 1:
                    result += " O ";
                    break;
                case 2:
                    result += " X ";
                    break;
            }

            if ((i + 1) % 3 == 0 && i != 0) {
                result += "\n";
            }
        }
        System.out.println(result);
    }

    public int checkWinner() {
        int totalPlayer1;
        int totalPlayer2;
        for (int[] line : winnerPlays) {
            totalPlayer1 = 0;
            totalPlayer2 = 0;
            for (int location : line) {
                if (map[location - 1] == 1) {
                    totalPlayer1++;
                }
                if (map[location - 1] == 2) {
                    totalPlayer2++;
                }
                if (totalPlayer1 == 3) {
                    return 1;
                }
                if (totalPlayer2 == 3) {
                    return 2;
                }
            }
        }
        return 0;
    }

    boolean checkDraw() {
        for (int i : map) {
            if (i == 0) {
                return false;
            }
        }
        return true;
    }

    protected void input1vs1(int player) {
        String position;
        do {
            System.out.printf("Jugador %d: ", player);
            position = keyboard.nextLine();
        }while (!validatePosition(position));
        int parsePosition = Integer.parseInt(position);
        setMap(parsePosition, player);
    }

    protected void switchPlayer() {
        if(actualPlayer == player1) {
            actualPlayer = player2;
        }else {
            actualPlayer = player1;
        }
    }

    protected void mainMenu() {
        System.out.println("TRES EN RAYA\n");
        System.out.println("1. Jugador vs Jugador");
        System.out.println("2. Jugador vs Máquina");
        System.out.println("3. Máquina vs Máquina");
        System.out.println("4. Salir");
        String choice;
        do {
            choice = keyboard.nextLine();
        }while (!validateMainMenu(choice));
        int choiceInt = Integer.parseInt(choice);
        switch (choiceInt) {
            case 1:
                play1vs1();
                break;
            case 2:
                playVsMachine();
                break;
            case 3:
                playVsMachines();
                break;
            case 4:
                System.exit(0);
                break;
        }
    }
}

